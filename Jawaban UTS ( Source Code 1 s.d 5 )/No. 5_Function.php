<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Function</title>
</head>

<body>
    <h1>No. 5_Function.PHP</h1>
    <?php

    function MenentukanSebuahNilai ($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik <br>";
        }
        if ($nilai >= 70 && $nilai < 85) {
            return "Baik</br>"; 
        }
        if ($nilai >= 60 && $nilai < 70) {
            return "Cukup</br>";
        }
        return "Kurang";
    }

    echo MenentukanSebuahNilai(98);
    echo MenentukanSebuahNilai(76);
    echo MenentukanSebuahNilai(67);
    echo MenentukanSebuahNilai(43);


    ?>
</body>

</html>

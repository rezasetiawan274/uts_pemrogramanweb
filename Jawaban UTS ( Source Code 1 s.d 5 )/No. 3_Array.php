<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kids dan Adults</title>
</head>

<body>
    <h1> No. 3_Array.PHP</h1>
    <?php
    $Anakanak = [ "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" ];
    $Dewasa = [ "Hopper", "Nancy", "Joyce", "Jonathan", "Murray" ];

    $totalAnakanak = count($Anakanak);
    $totalDewasa = count($Dewasa);

    echo 'Anakanak : ' ;

    foreach ($Anakanak as $key => $Anakanak) {
        echo '"' . $Anakanak . '"';
        if ($key < ($totalAnakanak - 1)) {
            echo ", ";
        }
    }
    echo '<br>Dewasa : ' ;

    foreach ($Dewasa as $key => $Dewasa) {
        echo '"' . $Dewasa . '"'; {
            if ($key < ($totalDewasa - 1)) {
                echo ", ";
            }
        }
    }

    ?>
</body>

</html>
